# Usecases

A *DAO* (Decentralized Autonomous Organization) can:

## Sign documents and contracts in a publicly proovable way:

* simple, static documents
* immutable smart contracts, eg: to pay automatically based on conditions or with multiple/custom approvals

## Pay:

* spot payment, eg: to external contributors
* regular payments automatically, eg: to employes
* vesting options, eg: to managers (delay funds unlocking with predefined functions, while allowing to vote with all funds)
* based on events & conditions (still verifiable & censor resistant if state is on-chain), eg. to suppliers or clients (fidelity points)

## Vote on governance proposals:

* to allow payments (like a multi-signature wallet)
* for internal processes/parameters/configuration updates
* to create & interact with smart contracts

## Create governance layers with sub-DAOs & multiple smart contracts

with complex but safe, semi-automatic and trackable approval processes & execution workflows

## Mint & Track its assets

Quasi for free with permissionless & neutral access, for example:

* B2C & B2B goods & data exchange with same level of security through the whole supply chain, eg: you can mint a collection with verified ids per production batch
* Automatically distribute & regulate access to (pre-paid or contract-based) digital goods, services, bounties, etc.
* Verify digital goods like org-signed open source code and its dependency trees + its verified usage if on-chain

Note: special-purpose private or regulated "distributed" ledgers may be created for specific applications, like retail supply-chains and CBDCs, but the blockchain (value-added) use cases fall apart when the protocol is permissioned, regulated & imposed by a special authory instead of permissionless and decentralized (governed by proovable, voluntary and distributed consensus). "Permissioned blockchains" are an oxymoron since they potentially create an infite barrier to entry for competition and centralize power to the "guardian" incumbents.

## Charge tokens automatically based on allowances in (co-)managed accounts

TODO

## Manage its infrasctrure

(additionally to the dApp which already lives on-chain)

* directly & automatically with on-chain payments, for:
  * general purpose, versatile hosting, eg: with Akash (Cosmos)
  * special purpose with mixed backends, eg: the DAO's social network space with SubSocial (Polkadot + IPFS storage)
* indirectly
  * by approving recurring payments to hosting providers
    (a 3rd party payment processor may be needed depending on the hosting provider accepted currencies)
  * by managing access keys or roles with NFTs or authentication modules/smartcontracts
  * by a SubDAO with all the above features but different scope, parameters, etc.. eg: with DA0_DA0 on Juno (Cosmos)
