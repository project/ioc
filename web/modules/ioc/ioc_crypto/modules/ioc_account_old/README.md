# IOC Crypto Account

This submodule allows fast & batch creation and importing of crypto accounts into your ioc site by integrating with a few external (open source) libraries and blockchain networks, currently:

- [bananojs](https://github.com/BananoCoin/bananojs) for [Nano](https://nano.org/) & [Banano](https://banano.cc) protocols
- [bananojs-hw](https://github.com/BananoCoin/bananojs-hw) (WIP) for Nano & Banano with hardware wallet support (like those sold by [Ledger.com](https://www.ledger.com/))
- [feather.js](https://github.com/terra-money/feather.js) for any [Cosmos](https://cosmos.network/) ecosystem app-chain (eg. [Juno](https://junonetwork.io/) for DAOs & smart contracts, [Stargaze](https://www.stargaze.zone/) for NFTs, [Osmosis](https://osmosis.zone/) for exchanging tokens, [Secret](https://scrt.network/) for privacy, etc.)

_Crypto accounts_ are NOT managed by Drupal but created "offline" and just imported to the site (the shareable public key part, ie. address) by means of a convenient single command (read more in <a href="#how">How does it work section</a>).

These can be used for your organization's internal accounting (when your site is an intranet) or to let anyone reward/promote content (or any entity) on your web site in a transparent way (crowdfunding, accepting donations, or providing a custodial bridge to paying your editors/contributors/authors).

**Note**:

* If you create accounts for your users (like centralized exchanges like Binance or Coinbase do), and even if you later share the private key of each account to the user, they should know that you (and anyone that gets to know the seed) can drain all the derived accounts at anytime.

* If instead you want each of your users in control, you don't need this submodule: just let them create their accounts with their favorite (trusted) wallet app and paste their addresses into a "account" storage entity type form (not "io_account"), or use the "**ioc_connect**" submodule (WIP) to do the same with 1 click.

## Requirements

* All of the external libraries used are based on [Node.js](https://nodejs.org/) so you need to have that installed on your machine. You can easily get it through Homebrew package manager with `brew install node`.

* The default script(s) provided use the [pass](https://www.passwordstore.org/) CLI utility to encrypt the generated seeds and private keys. You can get it (and its dependencies) with `brew install pass`.

* Optionally and by default, the script ends by importing accounts with [Drush](https://www.drush.org/): you should already have it responding to `iocd` or `ddev drush` or `drush` from your (ioc) project folder by now.

## Installation

* Since this is a submodule, you can install just by enabling it at https://ioc.ddev.site/admin/modules or by running `iocd en io_account` (assuming the parent 'ioc_crypto' module is installed and ioc aliases are in your shell configuration)
* Download the libraries listed above and move them inside a [root]/`wallets` folder (same level as your `web` folder), eg:

  ```
  gh repo clone BananoCoin/bananojs && mv ~/bananojs ~/ioc/wallets/bananojs
  gh repo clone BananoCoin/bananojs-hw && mv ~/bananojs-hw ~/ioc/wallets/bananojs
  gh repo clone terra-money/feather.js && mv ~/feather.js ~/ioc/wallets/feather.js
  ```

  Note: if you cloned the whole IOC project you already have them, defined as Git submodules.

* Move the provided script(s) inside a [root]`/scripts` folder, eg:

  ```
  mv -v ~/ioc/web/modules/ioc/ioc_crypto/modules/io_account/scripts/* ~/ioc/scripts/
  ```
  ..and make sure they are executable by you with `chmod -R 744 ~/ioc/scripts`

* Initialize your passwords' store with `pass init -p ioc`
  
  **NOTE**: in the provided default implementation all the accounts' private keys security will depend on this password, so please read the <a href="#security">security</a> section below for the appropriate setup.

<a id="how"></a>
## Usage

When you enable the module for the first time these entity configurations (defined in /config/optional) will be imported:

* a [storage](https://www.drupal.org/project/storage) entity type of bundle [io_account](https://ioc.ddev.site/admin/structure/storage_types/io_account/edit), that will hold the generated addresses in its fields

* a [Feeds](https://www.drupal.org/project/feeds)'s feed_type of bundle [csv__io_account](https://ioc.ddev.site/admin/structure/feeds/manage/csv__io_account) to map and import the addresses from a CSV to the storage entities above

* an [ECA](https://www.drupal.org/project/eca) model that will map new "io_account" storage entities to new entities (by default any content node) as they are created on the site.

### Account generation

When you run `iogen 10` from your ioc project folder:

1. one of the libraries is called (default is bananojs) to create **1 random seed** (the master private key, in the form of a hexadecimal string or a 'mnemonic', as defined in [BIP39](https://github.com/bitcoin/bips/blob/master/bip-0039.mediawiki)) and **10 private keys**, ie. sub-accounts derived from it (see [BIP44](https://github.com/bitcoin/bips/blob/master/bip-0044.mediawiki)), starting from index 1 or the next incremental ID of your site's storage entities.

2. The script backs up the seed and private keys as encrypted by your custom password (or OpenPGP key) with the _pass_ utility, copies the addresses to a CSV, puts it in the canonical public files folder and imports all existing feeds of type "csv__io_account" through a drush command

#### Notes:

* You can find more usage examples in the `generate_wallets.sh` script itself.

* Since we are using Drupal's public file storage for transparency, to avoid unwanted users to upload compatible CSV files with different addresses at the same path, you should make sure to set [appropriate Feeds' module permissions](https://ioc.ddev.site/admin/people/permissions/module/feeds) on the feed types that process `io_account` entities or that can upload to the same directory.

* When a feed type's *Fetcher* is "Upload file", you can enforce the path where user-provided files will be uploaded under *Fetcher settings > Upload directory*, when it's "Directory" (like in this case, cause we are uploading from the backend and just saying where to look at from the UI) you should probably restrict the permissions to create/edit/import feeds of this type to trusted roles.

* A good practice is to define a specific upload/search directory under *feeds/*, with subpath equal to the machine name of the feed type itself. **IOC canonical** notation for feed types' **machine names** is "source_inputtype__output_bundle", hence the path in this case is `public://feeds/io_csv__io_account`; canonical **labels** instead use human-readable labels with plurals and fancy arrows to explain the flow of data, in reversed i/o so that editorial lists are ordered by the entity bundle being imported: `IOC Accounts ← CSV file` in this case.

### Collection of Inputs

The new storage entities will then be available to receive funds by their address<sup>1</sup> fields. You may want to show them as links and/or QR codes ([live example](https://nano.to/kopeboy)) for your customers or donors in the page of a referencing product or article, or maybe just list them as internal business units with Views.

To update the balances from the blockchain to your site regularly or on-demand, a service is provided with the help of [http_client_manager](https://www.drupal.org/project/http_client_manager) module.
TODO

**NOTE:** in case the accounts are on the default Nano/Banano protocol, incoming transactions are queryable by the "[accounts_receivable](https://docs.nano.org/commands/rpc-protocol/#accounts_receivable)" action: that means you have to receive the XNO/BAN coins before being able to spend them. This is both a necessity and a feature, that allows on one side to keep the protocol free of any fee, inflation or incentive scheme (while reasonably protected from spam & secure), on the other to signal that you (the receiver) accept the input: you could integrate this in your business logic to optimize reporting, for example to receive funds only when a product/service was delivered. Read more about feeless spam resistance in this [Senatus' blog post](https://senatusspqr.medium.com/nanos-latest-innovation-feeless-spam-resistance-f16130b13598) and in the [official Nano documentation](https://docs.nano.org/protocol-design/spam-work-and-prioritization/).


### Disposal of Outputs

Let's say one account has received 100x

You (the io host) can then 

<a id="security"></a>
## Security

The recommended setup is using [pass](https://www.passwordstore.org/) together with [GnuPG](https://gnupg.org/) (a robust and recently revamped open source library for encryption), which can be used from the command line (already installed as a dependency) or through the optional GUIs ([GPG Suite](https://gpgtools.org/) on MacOS or [Gpg4win](https://www.gpg4win.org/) on Windows), so that any crypto account private key will be automatically encrypted with one of your keys following the [OpenPGP standard](https://datatracker.ietf.org/doc/html/rfc4880). This will also allow to register yourself as a verified io host on [ioc.xyz](https://ioc.xyz) (once the service will go live, **WIP**) to be discovered by other ioc sites, and sign the accounts CSV to prove you generated them, among other things.


### GPG Setup from the CLI

This should work on any operating system and is the recommended way.

1. Generate a key named 'ioc' with `gpg --quick-gen-key ioc`, and copy the returned fingerprint (the long uppercase string)

2. Initialize the password store with a 'ioc' folder using that key with `pass init -p ioc [YOUR_KEY]`

3. Test it by adding a custom password "test-pass" named 'test' under it with `pass insert ioc/test` and show it with `pass ioc/test`:

    * after providing your secret defined in step 1 you should get back "test-pass"; remove the test and anything under it with `pass rm -r ioc/test`
    * if you weren't asked for your secret key, it means the gpg-agent had it in cache: for improved security, edit the configuration to disable it by putting these lines in `~/.gnupg/gpg-agent.conf`:

        ```
        default-cache-ttl 0
        max-cache-ttl 0
        default-cache-ttl-ssh 0
        max-cache-ttl-ssh 0
        ```
      and quit it with `gpgconf -K all`

Use `gpg --help` and simulate the interactive process with the `gpg --generate-key -n` (the `-n` flag makes the command a dry-run) to see all the options, or type `man gpg` to read the full manual. Learn more about subkeys at https://wiki.debian.org/Subkeys.

### GPG Setup from the GUI:

These example instructions apply to the GPG Keychain app (installed with GPG Suite on MacOS). Using the GUI (even after using the CLI) makes it easier to do some optional things like publishing your key to the openpgp key server, adding your photo, setting up your default key, etc..

1. Create a new key pair with name 'ioc', your email, a very strong password, and select Key type = 'DSA and Elgamal' (see [RFC 8032](https://datatracker.ietf.org/doc/html/rfc8032))

2. If asked, you can publish your key (obviously the public part, since we are using asymmetric encryption) so that anyone will be able to verify you are the sender of your encrypted messages (and send secrets to you only) -- this will be used to automatically recognize you as a ioc host in case the same email is registered at ioc.xyz

3. Go to the settings ('GPG Suite' in system settings on MacOS) and:

   * disable the storing of the password in your OS keychain for added security (otherwise everything will depend on your operating system user login if an hacker knows where to search) and delete them if you had that enabled before (note that this will apply to all pre-existing keys so be sure you remember them),
  
   * for the same purpose, disable the password remember option to be prompted each time (or set only a few seconds),

   * select the ioc key as your default key for convenience in your tests (example commands should always make sure of using the right key fingerprint anyway)

4. Initialize the password store with a 'ioc' folder using that key with `pass init -p ioc`; you can happend a fingerprint to initialize with a specific key different from your default, but you will have to remove spaces if you copy-pasted from the UI (`pass init -p ioc ABCDEFINGERPRINT...`).

On the GPG Keychain app you can double-click a key to see its details, manage subkeys (gpg automatically adds a sub-key and shows it everytime you create one, to be able to revoke and substitute it from your main one in case it's compomised -- [learn more](https://wiki.debian.org/Subkeys)) add a photo, etc..

## Notes

1. the format of a private key and its associated public key (ie. the address where to send messages, coins or tokens) depends on the chosen blockchain protocol. Usually private keys follow the [BIP44](https://github.com/bitcoin/bips/blob/master/bip-0044.mediawiki) standard, so that a single master key ("seed", or "mnemonic") could be used to derive multiple accounts on multiple protocols.
Usually the dependent public key is prefixed by a chain identifier to form an address so that you identify the underlying protocol just by reading it, like "nano_" and "ban_" for Nano and Banano, or like "bc1" and "juno1" for Bitcoin and Juno, that follow the common Bech32 prefix (chain1). There are a lot of special cases though, like Cardano, that has multiple different bech32 prefixes for different types of addresses ("stake1" for your main staking account and "addr1" for all the change addresses, as in cash payment change -- see UTxO model), or Ethereum and many EVM-compatible chains which just keep the public key in HEXadecimal format (starting with "0x") as the address, so that you may think is the same account but it's not, has different (uncompatible) coins in it and potentially even different owners and types (like a smart contract): for this reason we don't support Ethereum and EVM protocols in the current form (read more [here](https://ethereum-magicians.org/t/chain-specific-addresses/6449/19) and [here](https://medium.com/@ajaotosinserah/mastering-addresses-in-ethereum-5411ba6c3b0f)).
