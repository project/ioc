# Usecases

A **crypto account** can *securely* (thanks to math, science, open source code & contributions) and without trust, permission or any third-party authority:

## Sign documents (or any artibrary data) in a publicly proovable way

* sign simple, static documents, eg: send a private and/or untamperable message
* sign immutable policies or smart contracts, eg: enable automatic payments based on conditions
* partially sign transactions from multi-signature accounts, with fixed rules.

## Pay & Receive payments to/from anyone with an internet connection

* spot payment without memo, eg: donation, private payment or any voluntary contribution
* spot payment with (on-chain) memo, eg: traditional ecommerce (custodial & contract-based)
* spot payment with on-chain metadata, eg: non-fungible tokens (NFTs), regulated goods, web3 commerce (decentralized)
* contract-based / automated payments, google "smart contracts" or read more at /ioc_dao/USECASES.md

## Vote on governance proposals:

* Manage or participate in a DAO or larger blockchain protocol, see more at /ioc_dao/USECASES.md

## Mint & Exchange & Track assets on public ledgers

Quasi for free with permissionless & neutral access, for example:

* F&F & B2C & B2B goods & personal data exchange with same level of security through the whole supply chain, eg: you can mint a collection with verified ids per production batch
* Automatically distribute & regulate access to (pre-paid or contract-based) digital goods, services, bounties, etc.
* Verify digital goods like org-signed open source code and its dependency trees + its verified usage if on-chain

Note: special-purpose private or regulated "distributed" ledgers may be created for specific applications, like retail supply-chains and CBDCs, but the blockchain (value-added) use cases fall apart when the protocol is permissioned, regulated & imposed by a special authory instead of permissionless and decentralized (governed by proovable, voluntary and distributed consensus). "Permissioned blockchains" are an oxymoron since they potentially create an infite barrier to entry for competition and centralize power to the "guardian" incumbents.
