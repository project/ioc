<?php

namespace Drupal\feeds_tamper_hex2dec\Plugin\Tamper;

use Drupal\tamper\Exception\TamperException;
use Drupal\tamper\TamperableItemInterface;
use Drupal\tamper\TamperBase;

/**
 * Plugin implementation for convert_encoding.
 *
 * @Tamper(
 *   id = "convert_hex_2_dec",
 *   label = @Translation("Convert hexadecimal to decimal"),
 *   description = @Translation("Converts string from hexadecimal to decimal."),
 *   category = "Text"
 * )
 */
class ConvertHex2Dec extends TamperBase {

  /**
   * {@inheritdoc}
   */
  public function tamper($data, TamperableItemInterface $item = NULL) {
    if (!is_string($data)) {
      throw new TamperException('Input should be a string.');
    }
    $decimal = base_convert($data, 16, 10);
    return $decimal;
  }

}
