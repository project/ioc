# IOC Modules
This module provides entities and processes for curating a list of drupal modules to support and share among ioc sites.
It provides:
- Taxonomy vocabulary <a href="/admin/structure/taxonomy/manage/module/overview">*modules*</a> with various fields and a themed display for useful curated info and links
- View <a href="/admin/modules/cc">*modules*</a> to list, filter, bulk edit and export (to CSV file) the module taxonomy terms
- JSON API resource <a href="/admin/config/services/jsonapi/resource_types/taxonomy_term--module/edit">*taxonomy_term--module*</a> with sensible defaults
- Feed types for importing module taxonomy terms from CSV upload (<a href="/admin/structure/feeds/manage/csv__module">*CSV file → Modules*</a>), or other ioc sites' modules json api (<a href="/admin/structure/feeds/manage/ioc__module">*IOC API → Modules*</a>)
- <a href="/admin/config/workflow/eca">ECA processes</a> for checking and maintaing the list of ioc modules against the ones installed or created locally.

## Importing ioc modules
The current list of ioc modules can be imported and updated by running the feed TODO provided with this module. By default, this won't delete local module terms not present in the feed, but you can change this setting by editing the <a href="/admin/structure/feeds/manage/ioc__module">*IOC API → Modules*</a> feed type.
