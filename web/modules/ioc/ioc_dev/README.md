# IOC Dev
This module provides common development helper modules and settings.
TODO:
Automatic (un)setting based on module (de)activation of:
- /admin/config/development/settings
- /admin/config/development/performance
- drush module
- core's config module
- (un)blocking of user 1 (TBD if with eca, drush script or paranoia module)
- config_split flags in settings.php #TBD
- disable css/js aggregation
