<?php

namespace Drupal\w3account;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\w3account\Entity\W3AccountType;
use Drupal\w3account\Entity\W3AccountTypeInterface;

/**
 * Provides dynamic permissions for Accounts of different types.
 *
 * @ingroup w3account
 */
class W3AccountPermissions {

  use StringTranslationTrait;

  /**
   * Returns an array of Account permissions.
   *
   * @return array
   *   The Account by bundle permissions.
   *   @see \Drupal\user\PermissionHandlerInterface::getPermissions()
   */
  public function generatePermissions() {
    $perms = [];

    foreach (W3AccountType::loadMultiple() as $type) {
      $perms += $this->buildPermissions($type);
    }

    return $perms;
  }

  /**
   * Returns a list of Account permissions for a given Account type.
   *
   * @param \Drupal\w3account\Entity\W3AccountTypeInterface $type
   *   The Account type.
   *
   * @return array
   *   An associative array of permission names and descriptions.
   */
  protected function buildPermissions(W3AccountTypeInterface $type) {
    $type_id = $type->id();
    $type_params = ['%type_name' => $type->label()];

    return array_map(static function ($perm) use ($type) {
        $perm['dependencies'][$type->getConfigDependencyKey()][] = $type->getConfigDependencyName();
        return $perm;
      },
      [
      "add $type_id accounts" => [
        'title' => $this->t('Add new %type_name accounts', $type_params),
      ],
      "view published $type_id accounts" => [
        'title' => $this->t('View published %type_name accounts.', $type_params),
      ],
      "view unpublished $type_id accounts" => [
        'title' => $this->t('View unpublished %type_name accounts.', $type_params),
        'restrict access' => TRUE,
      ],
      "view own unpublished $type_id accounts" => [
        'title' => $this->t('View own unpublished %type_name accounts.', $type_params),
      ],
      "edit own $type_id accounts" => [
        'title' => $this->t('Edit own %type_name accounts', $type_params),
      ],
      "edit any $type_id accounts" => [
        'title' => $this->t('Edit any %type_name accounts', $type_params),
        'restrict access' => TRUE,
      ],
      "delete own $type_id accounts" => [
        'title' => $this->t('Delete own %type_name accounts', $type_params),
      ],
      "delete any $type_id accounts" => [
        'title' => $this->t('Delete any %type_name accounts', $type_params),
        'restrict access' => TRUE,
      ],
    ]);
  }

}
