<?php

namespace Drupal\w3account;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\taxonomy\Entity\Term;

/**
 * Defines a class to build a listing of Account entities.
 *
 * @ingroup w3account
 */
class W3AccountListBuilder extends EntityListBuilder {

  // This is just a simple fallback of the provided 'accounts' View

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('ID');
    $header['type'] = $this->t('Type');
    $header['network'] = $this->t('Network');
    $header['address'] = $this->t('Address');
    $header['status'] = $this->t('Status');
    $header['changed'] = $this->t('Updated');
    $header['uid'] = $this->t('Author');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\w3account\Entity\W3Account $entity */
    $row['id'] = $entity->id();
    $row['type'] = $entity->type->entity->label();
    $row['network'] = $entity->get('network')->entity->label();
    $row['address'] = $entity->get('address')->value;
    $row['status'] = $entity->get('status')->value ? $this->t('Published') : $this->t('Unpublished');
    $row['changed']['data'] = $entity->get('changed')->view(['label' => 'hidden']);
    $username_options = [
      'label' => 'hidden',
      'settings' => ['link' => $entity->get('uid')->entity->isAuthenticated()],
    ];
    $row['uid']['data'] = $entity->get('uid')->view($username_options);
    return $row + parent::buildRow($entity);
  }



}
