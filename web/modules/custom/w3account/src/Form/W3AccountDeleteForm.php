<?php

namespace Drupal\w3account\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Account entities.
 *
 * @ingroup w3account
 */
class W3AccountDeleteForm extends ContentEntityDeleteForm {


}
