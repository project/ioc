<?php declare(strict_types = 1);

namespace Drupal\w3account\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\w3account\Entity\W3AccountInterface;
use Drupal\w3account\Entity\W3AccountType;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for Account edit forms.
 *
 * @ingroup w3account
 */
class W3AccountForm extends ContentEntityForm {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $user;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    $instance = parent::create($container);
    $instance->user = $container->get('current_user');
    return $instance;
  }

    /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    /** @var \Drupal\w3account\Entity\W3AccountInterface $entity */
    $entity = $this->entity;
    // Load the bundle.
    $bundle = W3AccountType::load($entity->bundle());

    // If explanation or submission guidelines are set, display them.
    if ($help = $bundle->getHelp()) {
      $form['help'] = [
        '#markup' => $help,
        '#weight' => -1000,
      ];
    }

    if ($this->operation == 'add') {
      $form['#title'] = $this->t('Add @type account', [
        '@type' => $bundle->label(),
      ]);
    }
    elseif ($this->operation == 'edit') {
      $form['#title'] = $this->t('Edit @type account #@id', [
        '@type' => $bundle->label(),
        '@id' => $entity->id(),
      ]);
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): int {
    $result = parent::save($form, $form_state);

    $message_args = ['%label' => $this->entity->toLink()->toString()];
    $logger_args = [
      '%label' => $this->entity->label(),
      'link' => $this->entity->toLink($this->t('View'))->toString(),
    ];

    switch ($result) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('New account %label has been created.', $message_args));
        $this->logger('test_acc')->notice('New account %label has been created.', $logger_args);
        break;

      case SAVED_UPDATED:
        $this->messenger()->addStatus($this->t('The account %label has been updated.', $message_args));
        $this->logger('test_acc')->notice('The account %label has been updated.', $logger_args);
        break;

      default:
        throw new \LogicException('Could not save the entity.');
    }

    $form_state->setRedirectUrl($this->entity->toUrl());

    return $result;
  }

}
