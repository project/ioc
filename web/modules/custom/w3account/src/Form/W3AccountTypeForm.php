<?php

namespace Drupal\w3account\Form;

use Drupal\Core\Entity\BundleEntityFormBase;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The form class for an Account type.
 */
class W3AccountTypeForm extends BundleEntityFormBase {

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Constructs the W3AccountTypeForm object.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   */
  public function __construct(EntityFieldManagerInterface $entity_field_manager) {
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\w3account\Entity\W3AccountTypeInterface $account_type */
    $account_type = $this->entity;
    if ($this->operation == 'add') {
      $form['#title'] = $this->t('Add account type');
    } else {
      $form['#title'] = $this->t('Edit %label account type', ['%label' => $account_type->label()]);
    }

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 60,
      '#default_value' => $account_type->label(),
      '#description' => $this->t('The human-readable label of this account type.'),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $account_type->id(),
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
      '#disabled' => !$account_type->isNew(),
      '#machine_name' => [
        'exists' => ['\Drupal\w3account\Entity\W3AccountType', 'load'],
        'source' => ['label'],
      ],
      '#description' => $this->t('A unique machine-readable name for this account type. It must only contain lowercase letters, numbers, and underscores. This name will be used for constructing the URL of the %account-add page.', [
        '%account-add' => $this->t('Add account'),
      ]),
    ];

    $form['description'] = [
      '#title' => $this->t('Description'),
      '#type' => 'textarea',
      '#rows' => 2,
      '#default_value' => $account_type->getDescription(),
      '#description' => $this->t('This brief (plain text) description will be displayed on the <em><a href="/account/add" target="_blank">Add account</a></em> page.'),
    ];

    $form['help'] = [
      '#title' => $this->t('Explanation or submission guidelines'),
      '#type' => 'textarea',
      '#rows' => 2,
      '#default_value' => $account_type->getHelp(),
      '#description' => $this->t('This text will be displayed at the top of the form when creating or editing accounts of this type.'),
    ];

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => t('Published by default'),
      '#description' => $this->t('Whether new accounts of this type should be published by default.'),
      '#default_value' => $account_type->getStatus(),
    ];

    return $this->protectBundleIdElement($form);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\w3account\Entity\W3AccountTypeInterface $account_type */
    $account_type = $this->entity;
    $account_type->setStatus((bool) $form_state->getValue('status'));
    $status = $account_type->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label account type.', [
          '%label' => $account_type->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label account type.', [
          '%label' => $account_type->label(),
        ]));
    }

    $fields = $this->entityFieldManager->getFieldDefinitions('account', $account_type->id());
    // Update the status field definition.
    // @todo Make it possible to get default values without an entity.
    //   https://www.drupal.org/node/2318187
    /** @var \Drupal\w3account\Entity\W3AccountInterface $account */
    $account = $this->entityTypeManager->getStorage('account')->create(['type' => $account_type->id()]);
    if ($account->isPublished() != $account_type->getStatus()) {
      $fields['status']->getConfig($account_type->id())->setDefaultValue($account_type->getStatus())->save();
    }
    $this->entityFieldManager->clearCachedFieldDefinitions();
    $form_state->setRedirectUrl($account_type->toUrl('collection'));
  }
}
