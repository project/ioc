<?php

namespace Drupal\w3account\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access control handler for the Account type entity.
 *
 * @see \Drupal\w3account\Entity\W3AccountType
 */
class W3AccountTypeAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    switch ($operation) {

      case 'view':
        return AccessResult::allowedIfHasPermissions(
          $account,
          [
            'view published accounts',
            'view published ' . $entity->id() . ' accounts',
            'view unpublished accounts',
            'view unpublished ' . $entity->id() . ' accounts',
            'view own unpublished accounts',
            'view own unpublished ' . $entity->id() . ' accounts',
            \Drupal::entityTypeManager()->getDefinition($entity->getEntityType()->getBundleOf())->getAdminPermission(),
            $entity->getEntityType()->getAdminPermission(),
          ],
          'OR'
        )
          ->cachePerPermissions()
          ->addCacheableDependency($entity);

      default:
        return parent::checkAccess($entity, $operation, $account);

    }
  }

}
