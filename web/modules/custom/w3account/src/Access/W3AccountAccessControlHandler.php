<?php

namespace Drupal\w3account\Access;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Account entity.
 *
 * @see \Drupal\w3account\Entity\W3Account.
 */
class W3AccountAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $user) {
    /** @var \Drupal\w3account\Entity\W3AccountInterface $entity */

    if ($user->hasPermission('administer accounts')) {
      return AccessResult::allowed()->cachePerPermissions();
    }

    switch ($operation) {

      case 'view':

        if (!$entity->isPublished()) {
          $permission = $this->checkOwn($entity, 'view unpublished', $user);
          if (!empty($permission)) {
            return AccessResult::allowed()
              ->cachePerPermissions()
              ->cachePerUser()
              ->addCacheableDependency($entity);
          }

          return AccessResult::allowedIfHasPermission($user, 'view unpublished accounts')
            ->cachePerPermissions()
            ->addCacheableDependency($entity);
        }

        return AccessResult::allowedIfHasPermissions(
          $user,
          [
            'view published accounts',
            'view published ' . $entity->bundle() . ' accounts',
          ],
          'OR'
        )
          ->cachePerPermissions()
          ->addCacheableDependency($entity);

      case 'update':

        $permission = $this->checkOwn($entity, $operation, $user);
        if (!empty($permission)) {
          return AccessResult::allowed()
            ->cachePerPermissions()
            ->cachePerUser()
            ->addCacheableDependency($entity);
        }
        return AccessResult::allowedIfHasPermissions(
          $user,
          [
            'edit accounts',
            'edit any ' . $entity->bundle() . ' accounts',
          ],
          'OR'
        )
          ->cachePerPermissions()
          ->addCacheableDependency($entity);

      case 'delete':

        $permission = $this->checkOwn($entity, $operation, $user);
        if (!empty($permission)) {
          return AccessResult::allowed()
            ->cachePerPermissions()
            ->cachePerUser()
            ->addCacheableDependency($entity);
        }
        return AccessResult::allowedIfHasPermissions(
          $user,
          [
            'delete accounts',
            'delete any ' . $entity->bundle() . ' accounts',
          ],
          'OR'
        )
          ->cachePerPermissions()
          ->addCacheableDependency($entity);
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral()->cachePerPermissions();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $user, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermissions(
      $user,
      [
        'administer accounts',
        'add accounts',
        'add ' . (string) $entity_bundle . ' accounts',
      ],
      'OR'
    );
  }

  /**
   * Check for given 'own' permissions.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to check for.
   * @param string $operation
   *   The operation to perform.
   * @param \Drupal\Core\Session\AccountInterface $user
   *   The user.
   *
   * @return string|null
   *   The permission string indicating it's allowed.
   */
  protected function checkOwn(EntityInterface $entity, $operation, AccountInterface $user) {
    /** @var \Drupal\w3account\Entity\W3AccountInterface $entity */
    $uid = $entity->getOwnerId();

    $is_own = $user->isAuthenticated() && $user->id() == $uid;
    if (!$is_own) {
      return NULL;
    }

    $entity_ops = [
      'view unpublished' => 'view own unpublished accounts',
      'update' => 'edit own accounts',
      'delete' => 'delete own accounts',
    ];
    $entity_permission = $entity_ops[$operation];
    if ($user->hasPermission($entity_permission)) {
      return $entity_permission;
    }

    $bundle_ops = [
      'view unpublished' => 'view own unpublished %bundle accounts',
      'update' => 'edit own %bundle accounts',
      'delete' => 'delete own %bundle accounts',
    ];
    $bundle_permission = strtr($bundle_ops[$operation], ['%bundle' => $entity->bundle()]);

    if ($user->hasPermission($bundle_permission)) {
      return $bundle_permission;
    }

    return NULL;
  }
}
