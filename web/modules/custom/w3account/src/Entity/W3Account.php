<?php

namespace Drupal\w3account\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\user\UserInterface;
use Drupal\w3account\Entity\W3AccountInterface;
use Drupal\user\EntityOwnerTrait;
// QR image related:
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\file\Entity\File;
use chillerlan\QRCode\QRCode as PHPQRCode;
use chillerlan\QRCode\QROptions;

/**
 * Defines the Account entity class.
 *
 * @ContentEntityType(
 *   id = "account",
 *   label = @Translation("Account"),
 *   label_collection = @Translation("Accounts"),
 *   label_singular = @Translation("account"),
 *   label_plural = @Translation("accounts"),
 *   label_count = @PluralTranslation(
 *     singular = "@count accounts",
 *     plural = "@count accounts",
 *   ),
 *   bundle_label = @Translation("Account type"),
 *   handlers = {
 *     "list_builder" = "Drupal\w3account\W3AccountListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\w3account\Access\W3AccountAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\w3account\Form\W3AccountForm",
 *       "edit" = "Drupal\w3account\Form\W3AccountForm",
 *       "delete" = "Drupal\w3account\Form\W3AccountDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\Core\Entity\Form\DeleteMultipleForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\w3account\Routing\W3AccountHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "accounts",
 *   admin_permission = "administer account types",
 *   entity_keys = {
 *     "id" = "aid",
 *     "bundle" = "type",
 *     "uid" = "uid",
 *     "label" = "label",
 *     "published" = "status",
 *   },
 *   links = {
 *     "collection" = "/admin/content/account",
 *     "add-form" = "/account/add/{account_type}",
 *     "add-page" = "/account/add",
 *     "canonical" = "/account/{account}",
 *     "edit-form" = "/account/{account}/edit",
 *     "delete-form" = "/account/{account}/delete",
 *     "delete-multiple-form" = "/account/delete",
 *   },
 *   bundle_entity_type = "account_type",
 *   field_ui_base_route = "entity.account_type.edit_form",
 *   fieldable = TRUE
 * )
 */
class W3Account extends ContentEntityBase implements W3AccountInterface {

  use EntityOwnerTrait;
  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'uid' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);
    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage): void {
    parent::preSave($storage);
    if (!$this->getOwner()) {
      // If no owner has been set explicitly, make the anonymous user the owner.
      $this->setOwnerId(0);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    if (!$update) {
      // Create the QR Image file, link it to the account, save the new account
      $file = $this->createQRCode();
      $this->set('qr', ['target_id' => $file->id()]);
      $this->save();
    } elseif ($this->getTarget() !== $this->original->getTarget()) {
      // Update the QR Image file (keep the reference w/o updating the account)
      $file = $this->createQRCode();
    }
  }

  /**
   * Create the qrcode and add a svg file.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The file entity.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function createQrCode(): EntityInterface {
    $qrcodeOptions = new QROptions([
      'outputType' => PHPQRCode::OUTPUT_MARKUP_SVG,
      'imageBase64' => FALSE,
    ]);
    $qrcode = new PHPQRCode($qrcodeOptions);
    $id = $this->id();

    $target = $this->getTarget();

    /** @var \Drupal\Core\File\FileSystemInterface $fileSystem */
    $fileSystem = \Drupal::service('file_system');
    $dir = 'public://accounts/qr';
    $fileSystem->prepareDirectory($dir, FileSystemInterface::EXISTS_REPLACE);

    $filePath = $dir . '/' . $id . '.svg';
    $qrcodeRenderResult = $qrcode->render($target);

    $file = File::create([
      'filename' => $id,
      'uri' => $filePath,
      'status' => 1,
      'uid' => 1,
    ]);
    $file->save();

    // /** @var \Drupal\file\FileUsage\FileUsageBase $file_usage */
    // $file_usage = \Drupal::service('file.usage');
    // $file_usage->add($file, 'ioc_account', $this->getEntityTypeId(), $this->id());
    // It seems like this only generates duplicate usage #TBD.

    /** @var \Drupal\file\FileRepositoryInterface $fileRepository */
    $fileRepository = \Drupal::service('file.repository');
    $fileRepository->writeData($qrcodeRenderResult, $filePath, FileSystemInterface::EXISTS_REPLACE);

    return $file;
  }

  /**
   * Deletes the QR Image file.
   */
  public function deleteQRFile() {

    /** @var \Drupal\file\FileUsage\FileUsageBase $file_usage */
    $file_usage = \Drupal::service('file.usage');
    $file = $this->getQRFile();
    $file_usage->delete($file, 'file', $this->getEntityTypeId(), $this->id());
    $file_usage->delete($file, 'w3account', $this->getEntityTypeId(), $this->id());

    /** @var \Drupal\Core\File\FileSystemInterface $fileSystem */
    $fileSystem = \Drupal::service('file_system');
    $dir = 'public://accounts/qr';
    $fileSystem->prepareDirectory($dir, FileSystemInterface::EXISTS_REPLACE);
    $id = $this->id(); // this account
    $filePath = $dir . '/' . $id . '.svg';
    $fileSystem->delete($filePath); // apparently this doesn't really delete
    $file->setTemporary(); // or file.settings make_unused_managed_files_temporary = TRUE

  }

  /**
   * {@inheritdoc}
   */
  public function getLabel() {
    return $this->get('label')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setLabel() {
    // Create a label from the address
    $address = $this->getAddress();
    if (strlen($address) > 20) {
      $short_address = substr($address, 0, 8) . '...' . substr($address, -8);
      $this->set('label', $short_address);
    } else {
      $this->set('label', $address);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getAddress() {
    return $this->get('address')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getTarget(): string {
    // Use the address when QR Target is empty
    return $this->get('qr_target')->value ?? $this->getAddress();
  }

  /**
   * {@inheritdoc}
   */
  public function setFile(File $file) {
    $this->set('qr', $file);
  }

  /**
   * {@inheritdoc}
   */
  public function getQRFile() {
    $fileData = $this->get('qr')->first()->getValue();
    $file = File::load($fileData['target_id']);
    return $file;
  }

  /**
   * {@inheritdoc}
   */
  public function getUpdatedTime() {
    return $this->get('changed')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setUpdatedTime($timestamp) {
    $this->set('changed', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }


  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['label'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Label'))
      ->setRequired(FALSE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['network'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Network'))
      ->setDescription(t('The network on which this account lives.'))
      ->setRequired(FALSE) // EVM networks require this to distinguish accounts
      ->setCardinality(1)
      ->setSetting('target_type', 'taxonomy_term')
      ->setSetting('handler_settings', [
        'target_bundles' => ['network' => 'network'],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['address'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Address'))
      ->setDescription(t('The payable web3 address.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -3,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->addConstraint('UniqueField'); // multiple EVM networks per address won't
    // be supported until they implement network-specific (unique) addresses.

    $fields['qr'] = BaseFieldDefinition::create('file')
      ->setLabel(t('QR Image'))
      ->setDescription(t('The scannable qrcode rendered as SVG file (automatically generated by the QR Target text).'))
      ->setSettings([
        'uri_scheme' => 'public',
        'file_directory' => 'accounts/qr',
        'file_extensions' => 'svg',
      ])
      ->setTranslatable(FALSE)
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['qr_target'] = BaseFieldDefinition::create('string')
      ->setLabel(t('QR Target'))
      ->setDescription(t('An optional qrcode target for deep links or preconfigured transactions (leave empty to use the plain address).'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Status'))
      ->setSetting('on_label', 'Published')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -1,
        'settings' => [
          'display_label' => FALSE,
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'label' => 'above',
        'settings' => [
          'format' => 'enabled-disabled',
        ],
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['balance'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('Balance'))
      ->setSettings(array(
        'type' => 'numeric',
        'unsigned' => TRUE,
        'min' => 0,
        'precision' => 39,
        'scale' => 0,
        'not null' => FALSE,
        'description' => t('The native coin balance in base denomination (integer with max 39 digits'),
      ))
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['receivable'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('Receivable'))
      ->setSettings(array(
        'type' => 'numeric',
        'unsigned' => TRUE,
        'min' => 0,
        'precision' => 39,
        'scale' => 0,
        'not null' => FALSE,
        'description' => t('The native coin receivable amount in base denomination (integer with max 39 digits'),
      ))
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Updated'))
      ->setDescription(t('The last time the account was edited or refreshed from the network.'))
      ->setDisplayConfigurable('view', TRUE);

    // YOU CAN ADD THIS MANUALLY TO SOME ACCOUNT TYPES FROM THE UI
    // $fields['active'] = BaseFieldDefinition::create('timestamp')
    //   ->setLabel(t('Last active'))
    //   ->setDescription(t('The last time the account transacted on the network.'))
    //   ->setDisplayOptions('view', [
    //     'type' => 'timestamp_ago',
    //     'label' => 'above',
    //     'weight' => -1,
    //   ])
    //   ->setDisplayConfigurable('view', TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Author'))
      ->setDescription(t('The user who added the account on this site.'))
      ->setSetting('target_type', 'user')
      ->setCardinality(1)
      ->setTranslatable(FALSE)
      ->setDefaultValueCallback(self::class . '::getDefaultEntityOwner')
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }
}
