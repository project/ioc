<?php

namespace Drupal\w3account\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
use Drupal\Core\Entity\EntityStorageInterface;

/**
 * Defines the Account type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "account_type",
 *   label = @Translation("Account type"),
 *   label_collection = @Translation("Account types"),
 *   label_singular = @Translation("account type"),
 *   label_plural = @Translation("accounts types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count accounts type",
 *     plural = "@count accounts types",
 *   ),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\w3account\Form\W3AccountTypeForm",
 *       "edit" = "Drupal\w3account\Form\W3AccountTypeForm",
 *       "delete" = "Drupal\w3account\Form\W3AccountTypeDeleteForm"
 *     },
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\w3account\W3AccountTypeListBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\w3account\Routing\W3AccountTypeHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\w3account\Access\W3AccountTypeAccessControlHandler",
 *   },
 *   admin_permission = "administer account types",
 *   bundle_of = "account",
 *   config_prefix = "account_type",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "status" = "status",
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/account_types/add",
 *     "edit-form" = "/admin/structure/account_types/{account_type}/edit",
 *     "delete-form" = "/admin/structure/account_types/{account_type}/delete",
 *     "collection" = "/admin/structure/account_types",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "help",
 *     "status",
 *   },
 * )
 */
class W3AccountType extends ConfigEntityBundleBase implements W3AccountTypeInterface {

  /**
   * The Account type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Account type label.
   *
   * @var string
   */
  protected $label;

  /**
   * A brief description of this Account type.
   *
   * @var string
   */
  protected $description;

  /**
   * Whether Accounts of this type should be published/enabled by default.
   *
   * @var bool
   */
  protected $status = FALSE;

  /**
   * Help information shown to the user when creating accounts of this type.
   *
   * @var string
   */
  protected $help = '';

  /**
   * {@inheritdoc}
   */
  public function id() {
    return $this->id;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function getStatus() {
    return $this->status;
  }

  /**
   * {@inheritdoc}
   */
  public function setStatus($status) {
    $this->status = (bool) $status;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getHelp() {
    return $this->help;
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);
  }

  /**
   * {@inheritdoc}
   */
  public static function postDelete(EntityStorageInterface $storage, array $entities) {
    parent::postDelete($storage, $entities);

    // Clear the Account type cache to reflect the removal.
    $storage->resetCache(array_keys($entities));
  }


}
