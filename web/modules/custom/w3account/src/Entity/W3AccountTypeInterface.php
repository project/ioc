<?php

namespace Drupal\w3account\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Storage type entities.
 */
interface W3AccountTypeInterface extends ConfigEntityInterface {

  /**
   * Get the default status for Account items of this type.
   *
   * @return bool
   *   The default status value.
   */
  public function getStatus();

  /**
   * Set the default status value.
   *
   * @param bool $status
   *   The default status value.
   */
  public function setStatus($status);

  /**
   * Get the Account type description.
   *
   * @return string
   *   The brief description of this Account type.
   */
  public function getDescription();

  /**
   * Gets the help information.
   *
   * @return string
   *   The help information of this storage entity type.
   */
  public function getHelp();

}
