<?php

namespace Drupal\w3account\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;
// Qr image related:
use Drupal\file\Entity\File;
use Drupal\Core\Entity\EntityStorageInterface;

/**
 * Provides an interface for defining Account entities.
 *
 * @ingroup w3account
 */
interface W3AccountInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE);

  /**
   * Gets the Account label.
   *
   * @return string
   *   The label of the Account.
   */
  public function getLabel();

  /**
   * Shortens the address to set the label.
   */
  public function setLabel();

  /**
   * Gets the Account address.
   *
   * @return string
   *   The web3 address of the Account.
   */
  public function getAddress();


  /**
   * Gets the current qrcode target.
   *
   * @return string
   *   The title.
   */
  public function getTarget(): string;

  /**
   * File Setter.
   *
   * @param \Drupal\file\Entity\File $file
   *   The file entity.
   */
  public function setFile(File $file);

  /**
   * Helper to get the QR file object.
   *
   * @return \Drupal\file\Entity\File $file
   */
  public function getQRFile();

  /**
   * Deletes the qr file linked to the account.
   */
  public function deleteQRFile();

  /**
   * Gets the Account update timestamp.
   *
   * @return int
   *   Updated timestamp of the Account.
   */
  public function getUpdatedTime();

  /**
   * Sets the Account update timestamp.
   *
   * @param int $timestamp
   *   The Account creation timestamp.
   *
   * @return \Drupal\w3account\Entity\W3AccountInterface
   *   The called Account entity.
   */
  public function setUpdatedTime($timestamp);

  // TODO network, address, refreshed from network timestamp, etc. TBD

}
