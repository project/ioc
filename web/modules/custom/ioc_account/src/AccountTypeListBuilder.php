<?php

declare(strict_types=1);

namespace Drupal\ioc_account;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of account type entities.
 *
 * @see \Drupal\ioc_account\Entity\AccountType
 */
final class AccountTypeListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header['id'] = $this->t('ID');
    $header['label'] = $this->t('Label');
    $header['description'] = $this->t('Description');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    $row['id'] = $entity->id();
    $row['label'] = $entity->label();
    $row['description'] = $entity->description;
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function render(): array {
    $build = parent::render();

    $build['table']['#empty'] = $this->t(
      'No account types available. <a href=":link">Add account type</a>.',
      [':link' => Url::fromRoute('entity.account_type.add_form')->toString()],
    );

    return $build;
  }
}
