<?php

declare(strict_types=1);

namespace Drupal\ioc_account;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;

/**
 * Provides a list controller for the account entity type.
 */
final class AccountListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header['id'] = $this->t('ID');
    $header['type'] = $this->t('Type');
    $header['label'] = $this->t('Label');
    $header['status'] = $this->t('Status');
    $header['changed'] = $this->t('Changed');
    $header['uid'] = $this->t('Author');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    /** @var \Drupal\ioc_account\Entity\Account $entity */
    $row['id'] = $entity->id();
    $row['type'] = $entity->type->entity->label();
    $row['label'] = $entity->get('label')->value;
    $row['status'] = $entity->get('status')->value ? $this->t('Enabled') : $this->t('Disabled');
    $row['changed']['data'] = $entity->get('changed')->view(['label' => 'hidden']);
    $username_options = [
      'label' => 'hidden',
      'settings' => ['link' => $entity->get('uid')->entity->isAuthenticated()],
    ];
    $row['uid']['data'] = $entity->get('uid')->view($username_options);
    return $row + parent::buildRow($entity);
  }
}
