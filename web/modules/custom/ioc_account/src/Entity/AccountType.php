<?php

declare(strict_types=1);

namespace Drupal\ioc_account\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
use Drupal\Core\Entity\EntityStorageInterface;

/**
 * Defines the Account type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "account_type",
 *   label = @Translation("Account type"),
 *   label_collection = @Translation("Account types"),
 *   label_singular = @Translation("account type"),
 *   label_plural = @Translation("accounts types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count accounts type",
 *     plural = "@count accounts types",
 *   ),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\ioc_account\Form\AccountTypeForm",
 *       "edit" = "Drupal\ioc_account\Form\AccountTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\ioc_account\AccountTypeListBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\ioc_account\Access\AccountTypeAccessControlHandler",
 *   },
 *   admin_permission = "administer account types",
 *   bundle_of = "account",
 *   config_prefix = "account_type",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/account_types/add",
 *     "edit-form" = "/admin/structure/account_types/manage/{account_type}",
 *     "delete-form" = "/admin/structure/account_types/manage/{account_type}/delete",
 *     "collection" = "/admin/structure/account_types",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *   },
 * )
 */
final class AccountType extends ConfigEntityBundleBase implements AccountTypeInterface {

  /**
   * The machine name of this account type.
   * @var string
   */
  public string $id;

  /**
   * The human-readable name of the account type.
   * @var string
   */
  public string $label;

  /**
   * A brief description of the account type.
   * @var string
   */
  public string $description;

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);
  }

  /**
   * {@inheritdoc}
   */
  public static function postDelete(EntityStorageInterface $storage, array $entities) {
    parent::postDelete($storage, $entities);

    // Clear the Account type cache to reflect the removal.
    $storage->resetCache(array_keys($entities));
  }
}
