## INTRODUCTION

The IOC Account module provides the "account" entity type and various configurations to store, update and display information about web3 (on-chain, crypto) accounts on your ioc site.
The primary use cases for this module are:

- **Accounting / Crypto Dashboard or Fund**: track accounts and web3 assets from a single (offline or online) UI
- **Transparency / Decentralization**: make your site prompt/react-on transactions to/by any (registered or anonymous) user with an internet connection securely
- **Ecommerce**: manage multiple accounts for free to accept feeless worldwide payments and create automatic business flows

## ARCHITECTURE

### Account entity

A bundleable entity holding basic info about a specific web3 account. Create new bundles (ie. account types) to define different fields and processes for different crypto protocols. Base fields (created by default and shared by all accounts) are:

- **type**: the account type (canonically corresponding to the crypto protocol) is used to define common methods & fields among networks
- **network**: the taxonomy term reference of the web3 network hosting the account (this depends-on a "network" taxonomy vocabulary as defined by the main module *ioc_crypto* configuration)
- **address**: the web3 address identifying the account on its network (not enforced as unique string until all relevant networks won't use chain-specific addresses - blame EVM networks)
- **balance**: the integer number of native coin available on the account
- **receivable**: the integer number of native coin ready to be received at any time by the account (like Nano's receivable, Cosmos', Cardano's or most PoS networks' rewards)
- **uid**: the internal id of the site user who added the account on site (defaults to 0 for the fictional anonymous user)
- **aid**: the optional but unique account index, used for io host's operations on custodial accounts (read about IO Account submodule below)
- **label**: optional human-friendly text to recall the account on site (if empty, a shortened address will be used)
- **status**: boolean to indicate whether the account is published/enabled or not (a specific permission is available to show disabled accounts to the author only)
- **changed** (Updated): the last time the account was edited on site
- **active**: the last time the account was edited (has transacted) on the network
- **qr**: a scannable qrcode (SVG) file pointing to the account's qr_target field (useful for mobile wallets' deep linking)
- **qr_target**: an editable text field (defaulting to the account's address) to be rendered as a qrcode

### Views & ECA

Pre-configured Views and ECA models are provided in the config/optional folder and will be imported on first install for your convenience.

Views:

- a
- b
- c

ECA:

- a
- b
- c

### Currently supported protocols

| Protocol | Example Networks | Features | Example integration |
|----------|------------------|----------|---------------------|
| Nano | Nano, Banano.. | Sovereign & efficient payments with no fees, no inflation and instant confirmation*; ideal for pay per view, micro-donations/services, promoting content, distributing user credits | Manage disposable accounts for payments, collect rewards on user-generated content, tandem with local or sibling networks for special purposes (like Banano for banning/demoting content) |
| Cosmos | Cosmos Hub, Osmosis, Juno, Secret, Stargaze.. | The most flexible, easiest and cheapest smart contracting ecosystem with distributed governance tools, multi-network DEXes & NFT markets, optional privacy, permissioned & permissionless stablecoins, etc.. | Map Drupal groups to DAOs, authorization grants and group accounts, distribute credits as voting rights or NFTs, accept a wide range of tokens, manage critical data on-chain with optional privacy, distribute salaries with automatic vesting, etc. |

*Both Nano and Cosmos protocols were integrated first because they have "instant" finality of transactions, ie. a deterministic consensus algorithm (contrary to probabilistic ones of the protocols in consideration below): as soon as a transaction message is verified by the network of node operators and added to the distributed ledger it is considered final, settled and non-revertible. This usually happens in less than a second on Nano and a few seconds on Cosmos, depending mainly on network latency.

### Additional protocols in consideration

Reach out to ioc.xyz social channels for suggestions & feedback 🙏🏻

| Protocol | Example Networks | Features | Example integration |
|----------|------------------|----------|---------------------|
| EVM | Ethereum, Binance, Polygon, Avalanche C, Fantom, Celo.. | The most liquid & mature smart contracting ecosystem | Track and access a wide range of wallets, assets, and stablecoins, suggest interactions and react to a lot of existing smart contracts with common methods |
| Dotsama | Polkadot, Kusama and their parachains | Another flexible but newer ecosystem, similar to & connecting both EVM and Cosmos networks | TBD, potentially same usecases as with Cosmos & EVM chains |
| Solana | Solana | The fastest & most transacting network, perfect for high-frequency & complex financial applications as well as games and smart contracts | Like the above but with a more opinionated one-fits-all approach |
| Cardano | Cardano, Milkomeda, Algorand C | The most secure & stable network, with parallel (UTxO) execution of simple smart contracts written in a purely functional language | Economic research & public policy applications with high assurance |

## USAGE

- **Users** may import their crypto accounts manually by copy-pasting their addresses for each available network, or by "connecting" (posting) their browser-based wallets to the site thanks to the (TODO) javascript single directory component provided in the IOC Connect submodule.
- **IO Hosts** may generate & import custodial accounts in-bulk thanks to commands provided by the IO Custody submodule.

In both cases, since an account is really an external entity whose info will be queried from its specific network's API and providers, you may want to **create a wallet content type** yourself (see the example "IO Wallet" imported by the IO Custody submodule) to link multiple accounts together and display aggregate & human-friendly information about them (like price-converted asset values, historical information with revisions, etc.).

While any account entity may be manually referenced by a generic wallet entity on your site, is useful to keep in mind that usually a blockchain "wallet" corresponds to the cryptographic key that can derive multiple accounts of the same protocol (which is just a convention on the algorithms used to derive children account keys and addresses as in asymmetric cryptography).

Here are a just a few examples of wallet types and account types that you may define in Drupal UI or by code:

- a generic *Wallet* or *Portfolio* content type that may link accounts of any type (or even other wallets to create groups) and show the total value in "X" currency of assets held on multiple networks
- a *cosmos* wallet that may reference only *cosmos* accounts, which might have a *network* field that can only reference networks of *protocol* "cosmos" and other fields specific to cosmos features (like *group_address*, *authz_granter*, etc..)
- a *cardano* wallet could have an additional "stake_key" field to allow automatic linking of children accounts and reference an unlimited amount of *cardano_pay_addr* accounts that show balances in ADA denomination
- etc...

TODO usage of endpoints, providers, etc.

### Technical note on "wallets" vs "accounts"

The technical definition of an "account" or "wallet" varies between different blockchain protocols, for example:

- a Nano wallet may use the same public & private key to manage an account on the Nano network and one on the Banano network (a fork of Nano): their addresses will share the same public key string but have a different prefix ('nano_' vs 'ban_')
- a Cosmos wallet like Keplr may manage an account on each compatible network (created with the Cosmos SDK) with a single public-private key pair: addresses will have common characters (coming from the public key) but different prefixes and checksum suffixes
- a Cardano's wallet "stake" key or a Bitcoin's public key will create & manage new "accounts" for each UTxO with completely different addresses (only sharing the "addr1" or "bc1" prefix).
- a Metamask or similar EVM wallet may manage an account on each EVM-compatible network with the same key pair (with most networks using the same address as well)
- an hardware wallet may derive multiple private & public keys (one or more for each network) from a single master key

This means that accounts of type *nano*, *cosmos*, *evm* technically allow to automatically create/associate sibling accounts and a common parent wallet (since the wallet's public key is contained or easily obtainable from the account's address and network), *cardano* and *bitcoin* (and similar UTxO protocols') accounts can't be associated without knowing the common public key, and all accounts on known networks may be associated by knowing their common "master" public key (a.k.a. extended public key): **keep this in mind before (un)publishing your wallets if you care about (transparency)privacy.**

## REQUIREMENTS

This module depends on other Drupal contrib modules as well as local ioc modules (as listed in the ioc_account.info.yml file) which will be automatically downloaded & installed.

The IO Custody submodule has additional dependencies which must be added manually as explained in its README.

## INSTALLATION

Install as you would normally install a contributed Drupal module.
See: <https://www.drupal.org/node/895232> for further information.

## CONFIGURATION

This module hasn't any configuration form but it provides optional configuration files which will be imported **automatically** during the first install **if** configurations with the same names aren't already present on your site: you can see all of them in the config/option folder of this module.

Another optional but manual and suggested configuration is to change the default treatment of unused files by Drupal from "Permanent" to "Temporary": since an SVG file (QR Image field) will be created by this module for every account (in the public://accounts/qr folder), it is recommended to let cron automatically delete unused files (ie. after an Account entity was deleted); to apply this behaviour on all future files run `iocd cset file.settings make_unused_managed_files_temporary TRUE` (or drush cset [..same string] if you are not using DDEV and ioc aliases in your terminal). You may delete existing unused files manually from your <https://ioc.ddev.site/admin/content/files> or with the help of other contrib modules like *auditfiles*.

## MAINTAINERS

Current maintainers for Drupal 10:

- Lorenzo Giovenali (kopeboy) - <https://www.drupal.org/u/kopeboy>
