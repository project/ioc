**IOC is a W3MS (Web3 Management System) for decentralized collaboration.**

It is based on [Drupal](https://www.drupal.org) served by [Docker](https://www.docker.com/) containers and managed through [DDEV](https://ddev.com/) and [Drush](https://drush.org) for robust & fast local development.

It connects to various blockchains and other ioc sites through pre-configured APIs & RPCs that you can orchestrate with your custom business logic from the UI (often thanks to [ECA](https://ecaguide.org/) models), and extend with open source modules and recipes (configuration sets) from the **drupal** and **ioc** communities, or your own code.

This repo contains the complete set that is used on the live, canonical implementation at [ioc.xyz](https://ioc.xyz) (a community-owned content & services hub about crypto currencies, blockchains and Web3 in general), hence you can use it to validate ioc.xyz yourself or as a starting point to create your own web3 site. Find typical use cases and a list of ready-made recipes (with point & click installations for non-technical users) at [ioc.xyz/byo_](https://ioc.xyz/byo_). Check the documentation section and join the [Telegram group](https://t.me/ioc_xyz) to ask questions and provide feedback.

🙏🏻
