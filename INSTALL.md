# ioc installation instructions

## Quickstart

⏱️ ~5-10 mins

_This is currently tested only on MacOS with default terminal shell (zsh):_ but you can check the install.sh script file to make easy adjustments.

Clone this repository and from inside it (`cd ioc`) run the install script with `. ./scripts/install.sh` in your terminal. If you don't have the permission to execute it, set all of them with `chmod 744 scripts/install.sh`, so that you (the owner) can read & write & execute (4+2+1=7), the group (of your computer network, usually "staff") and everyone else can only read (4).

## Jump to

- [CLI Aliases](#aliases)
- [CLI Commands tips](#commands)

## Build it yourself with step-by-step instructions

⏱️ ~10-20 mins

Example commands are provided for MacOS (using the Terminal) and the canonical setup; follow the links for more options and other platforms (Linux, Windows, Gitpod and GitHub Codespaces).

1. Use [Homebrew](https://brew.sh/) (general purpose macOS/Linux package manager) for the easiest & most reliable installation process; install it with:

   `/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"`

   (or check the latest instructions at their site)

2. Install [Composer](https://getcomposer.org/), the package manager used by [Drupal](https://www.drupal.org) and its modules, with:

   `brew install composer`

3. Install [Docker](https://www.docker.com/) with:

   `brew install docker`

   (learn more about [Docker on DDEV docs](https://ddev.readthedocs.io/en/latest/users/install/docker-installation/))

4. Add [Colima](https://github.com/abiosoft/colima) for better performance with:

   `brew install colima`

5. ..and start it with:

   `colima start --cpu 4 --memory 6 --disk 100 --vm-type=qemu --mount-type=sshfs --dns=1.1.1.1`

6. [Install DDEV](https://ddev.readthedocs.io/en/latest/users/install/ddev-installation/) with:

   `brew install ddev/ddev/ddev`

7. Initialize a locally-trusted certificate to interact with your site without your browser complaining with:

   `mkcert -install`

8. Add ioc command aliases for convenience in your shell configuration file
   (in case you're using the default _zsh_ on MacOS Terminal you can open the hidden file with `cd && open .zshrc`, add these lines and save it)

       # ioc aliases
       alias iocc='ddev composer'
       alias iocd='ddev drush'
       alias iogen='./scripts/generate_wallets.sh'

   Restart your terminal to make it recognize the aliases (or run `source ~/.zshrc` in each terminal window).

9. Install [pass](https://www.passwordstore.org/) password manager to automatically save your site's crypto wallets as encrypted (and outside of the web-accessible folder) when needed, with:

   `brew install pass`

10. Create a folder for holding your "ioc" (or "your_name") project and prepare standard ddev settings inside of it with:
    
    `mkdir ioc && cd ioc && ddev config --project-type=drupal10 --docroot=web --create-docroot`

11. Download [Drupal](https://www.drupal.org), [Drush](https://drush.org) and their dependencies using [Composer](https://getcomposer.org/) package manager, with:

    `iocc create drupal/recommended-project -y && iocc require drush/drush`

12. Install and login into your (blank) ioc site with:

    `iocd si standard --site-name=ioc --account-name='io' --account-pass='io' --locale=en -y`

Go to your [ioc.ddev.site](https://ioc.ddev.site) (or launch it with `ddev launch`) to login and optionally change your password (if you installed without specifying a username and password you can get a one-time login url with `iocd uli` to reset them).

13. Enable the modules you want from https://ioc.ddev.site/admin/modules or read on..

(the quickstart procedure above will enable for you all ioc-provided modules with `iocd en ioc_all -y`)

## Adding modules

There are many thousands contributed open source (& free) modules already available on [Drupal/projects](https://www.drupal.org/project/project_module) and [GitLab/drupal/projects](https://git.drupalcode.org/explore/projects), but we try to keep an up-to-date list of recommendations and "recipes" (bundles of configuration) at [ioc.xyz/ioc/modules](https://ioc.xyz/ioc/modules), with one-liner installation commands and ioc-relevant notes.
To get that same list inside your local ioc environment, perhaps edit it and contribute back to the community, [enable](https://ioc.ddev.site/admin/modules/install) the _IOC Modules_ module first and then visit its [admin page](https://ioc.ddev.site/admin/modules/cc).

### Any module can be installed (locally) with two sequential commands in your terminal

* When it's already hosted on Drupal (at drupal.org/project/MODULE_MACHINE_NAME), like mature and general purpose modules:

   `iocc require drupal/machine_name && iocd en machine_name`

   which is equivalent (without ioc aliases) to:

   `ddev composer require drupal/machine_name && ddev drush en machine_name`

* When it's only on your local machine (like one you are developing), you should first tell composer where it is by adding its *path* to the start of your (root) site's *ioc/composer.json* repositories section, like:

            "repositories": [
                {
                    "type": "path",
                    "url": "web/modules/ioc/your_module"
                },
                {
                    "type": "composer",
                    "url": "https://packages.drupal.org/8"
                }
            ],

   ..then run the usual command above, and `iocd pmu machine_name` to uninstall it.
   Learn more at [Drupal/docs/develop](https://www.drupal.org/docs/develop).

__Note__: you can directly enable anything that has a compatible _your_module.info.yml_ file at _/web/modules/xxx/your_module/_  with `iocd en your_module`, but you will need [Composer](https://getcomposer.org/) and a _web/modules/xxx/your_module/_`composer.json` file to automatically manage dependencies and sharing your code on [Drupal](https://www.drupal.org), [Packagist](https://packagist.org/) or [__ioc.xyz__](https://ioc.xyz).

### You should never install a module on a live site

..and actually you can't (TODO) if you enabled `ioc_live` module for setting your site up for production!
First test locally, then deploy to your live, production instance (TODO more help on this..).

## CLI commands tips

* `colima start` and `colima stop` can be run from anywhere to start/stop your docker server.
* All other commands should be run from inside your ioc (or your_project_name) folder
* See the list of available commands for your site (each module can add its own) with `iocd list`
* See the list of available commands for your server with `ddev help`
* See help text for each command with `iocd COMMAND --help` or `ddev COMMAND -help`, or with the shorter flag alias `-h`.
