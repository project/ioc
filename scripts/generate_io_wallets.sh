#!/bin/sh

# Example of generating crypto accounts in batch on supported blockchains
# from a random private key (ie. wallet) and importing them on your site.
# See the ioc_crypto and its submodules readme for usecases and instructions.

# Currently supported protocols (ie. account types): Nano, Cosmos
# Supported networks per protocol:
# - Nano: Nano, Banano
# - Cosmos: Cosmos Hub, Osmosis, Juno, Secret, Stargaze, Terra
# Hence 8 accounts will be created for every generated wallet.
# External libraries used per protocol:
# - Nano: https://github.com/BananoCoin/bananojs
# - Cosmos: https://github.com/terra-money/feather.js

# Usage (with CLI alias, see ioc/INSTALL.md#ioc-aliases):
## `iogen 100`: generate 100 wallets, linked to 100 new accounts per network
## `iogen 200 100` to generate 100 wallets from id 101 to 200:
## this can be used to update existing accounts by id
## or to work in custom batches (eg. with rotating io hosts).
## To reuse ids after some account entities were deleted on your ioc site,
## set the next id to 101 with `iocd entity_id_tools:next_id account 101`.
## To check the current last id type `iocd entity_id_tools:last_id account`.
## WARNING: remember to check coin balances before overriding existing accounts!

x=$1
last=$(ddev drush entity_id_tools:last_id account) # see drupal.org/i/3387329

## Confirm the 2nd argument before overriding keys and updating accounts:
if [[ ! -z "$2" ]] && [[ $2 -lt $last ]]; then
    # "last entity ID is $last.."
    read -p " ..but you are starting from ${2}:
 Are you sure you want to override existing keys? [y/N] " yn
    case $yn in
        y  ) echo;;
        *  ) echo "exiting..."; exit;;
    esac

fi

id=${2:-$last}
from=$(( id + 1 ))
to=$(( x + id ))
csv="io_accounts" # generated chain addresses will be exported here
path="web/sites/default/files/feeds/" # canonical folder for public feeds' files
### equivalent of public://feeds/ on the UI

## Generate a seed and store the response in a .gpg encrypted file:
### Assuming the host has already initialized the 'ioc' key with install script

cd wallets/bananojs/
## take only the key, ie. the last 64 chars of the response:
seed=$(node main.js getseed | tail -c -65)
## add it to password storage, ecrypted with your gpg key (double prompt):
{ echo $seed; echo $seed;} |  pass insert ioc/seed/${from}_${to} -f
## without -f(orce) flag it would fail silently in case of existing pass

echo "⬆ [ENCRYPTED] seed: retrieve it with 'pass ioc/seed/${from}_${to}'.\n"

## Derive private keys for $x entities, starting from last $id or 0

echo "Generating ${x} keys & addresses, this may take ~ $((x/5)) seconds..."

for (( n=$from; n<=$to; n++ )) # account index 0 is reserved for the host TODO
do
    key=$(node main.js bgetprivatekey $seed $n | tail -c -65)
    # nano & banano chain share the same private key (given the same seed)
    # because we are not using HD derivation (ie. no hardware wallet support)
    # see https://github.com/BananoCoin/bananojs-hw TODO

    # override existing key if exists with -f (default would fail silently)
    { echo $key; echo $key; } | pass insert ioc/key/$n -f

    echo "↑ [encrypted] index $n key"

## Generate account from each private key

    acc=$(node main.js bgetaccount $key | tail -c -61)
    ### we keep only the common part of the address, without the ban_ prefix

## Format address and append to CSV file for ioc site import
    echo "${n},ban_${acc},nano_${acc}" >> $csv.csv
    # 1 private key : 1 account : 2 chains : 2 addresses

done

echo "\n⬆ Saved ${x} encrypted keys: retrieve them with 'pass ioc/key/[ID]'\n"

## Move the csv to the default (public files) folder for Feeds' imports

cp ${csv}.csv ../../${path}${csv}.csv
mv ${csv}.csv ../${csv}_${from}_${to}.csv
echo "Exported banano and nano addresses to ${path}${csv}.csv for site import,"
echo -e "and copied to wallets/${csv}_${from}_${to}.csv for backup.\n"


## Import accounts to ioc site with Feeds module

### On a new site, the ioc_crypto:ioc_account module's install procedure should
### create a new feed of type "csv__ioc_account", which will automatically
### import the ioc_accounts.csv every 3 hours from the canonical path.
### In case a feed of that type doesn't exist, you can create one from the UI at
### /feed/add/csv__ioc_account with path "public://feeds/ioc_modules.csv".
### You can edit the feed type to change the import frequency, the mapping
### between the source and the accounts' fields, or anything!

### To import existing feeds of this type manually with this script (uncomment):

ddev drush feeds:import csv__io_account
