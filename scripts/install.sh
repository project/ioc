!/bin/bash
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)" \
&&
brew install composer docker colima ddev/ddev/ddev pass
&&
mkcert -install
&&
colima start --cpu 4 --memory 6 --disk 100 --vm-type=qemu --mount-type=sshfs --dns=1.1.1.1
&&
ddev drush si standard --site-name=ioc --account-name='io' --account-pass='io' --locale=en -y
&&
echo "
# ioc ddev aliases
alias iocc='ddev composer'
alias iocd='ddev drush'
alias iogen='./scripts/generate_wallets.sh'
" >> ~/.zshrc
&&
chmod 744 generate_wallets.sh
&&
ddev composer require ioc/ioc_all -y && ddev drush en ioc_all -y && ddev launch

# WIP key generation
